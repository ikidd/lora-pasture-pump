#include "heltec.h"
#include "Arduino.h"
#include <WiFi.h>
#include "BarK3.h"

#include <ArduinoJson.h>
#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

#include "EspMQTTClient.h"

EspMQTTClient client(
  "LTE-Router",
  "",
  "192.168.8.10",  // MQTT Broker server ip
  "",   // Can be omitted if not needed
  "",   // Can be omitted if not needed
  "LoRaGW"      // Client name that uniquely identify your device
);

boolean gotpacket;
String packet;
String rssi;
String topic = "farm/";
int recipient = LoRa.read();          // recipient address
byte sender = LoRa.read();            // sender address
byte localAddress = 0xFF;     // address of this device

void setup() {
 
  Serial.begin(115200);

  Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/, true /*LoRa use PABOOST*/, BAND /*LoRa RF working band*/);
  logo();
  gotpacket = false;
  LoRa.onReceive(onReceive);
  LoRa.receive();
  client.enableDebuggingMessages(); // Enable debugging messages sent to serial output
 
}
 
void loop() {

  client.loop();

  
  if (gotpacket){
    gotpacket = false;

    DynamicJsonDocument doc(1024);    
    deserializeJson(doc, packet);
    JsonObject root = doc.as<JsonObject>();
    String topic = "farm/";
    
    for (JsonPair kv : root) {
        String k = kv.key().c_str();
        String v = kv.value().as<char*>();
//        Serial.print(k);
//        Serial.print(":");
//        Serial.println(v);
        client.publish(topic + String(sender, HEX)+ "/" + k, v);
        client.publish(topic + String(sender, HEX)+ "/rssi", rssi);
    }

  }

}

void logo(){
  Heltec.display -> clear();
  Heltec.display -> drawXbm(0,5,logo_width,logo_height,(const unsigned char *)logo_bits);
  Heltec.display -> display();
}

void onConnectionEstablished()
{
//  // Subscribe to "mytopic/test" and display received message to Serial
//  client.subscribe("mytopic/test", [](const String & payload) {
//    Serial.println(payload);
//  });
//
  // Subscribe to "mytopic/wildcardtest/#" and display received message to Serial
//  client.subscribe("farm/command/#", [](const String & topic, const String & payload) {
//    Serial.println(topic + ": " + payload);
//
//    if (topic == "farm/command/executed"){
//      if (payload == "0"){
//        String c;
//        String d;
//        client.subscribe("farm/command/destination",[](const String & dest){
//          d = dest;
//        });       
//        client.subscribe("farm/command/command",[](const String & command){
//          c = command;
//        });
//        sendcommand(d,c);
//        client.unsubscribe("farm/command/destination");
//        client.unsubscribe("farm/command/command");  
//        client.publish("farm/command/executed", "1");     
//      }
//    }
//  };
//
// Publish a message to "mytopic/test"
//  client.publish("farm/gateway", "Gateway connected"); // You can activate the retain flag by setting the third parameter to true
//
//  // Execute delayed instructions
//  client.executeDelayed(5 * 1000, []() {
//    client.publish("mytopic/test", "This is a message sent 5 seconds later");
//  });
}
void sendcommand(byte destination, String command)
   {

    StaticJsonDocument<200> doc;

    doc["cmd"] = command;
    
    String packet;
    serializeJson(doc, packet);
    Serial.println("Sending JSON: " + packet);
    Serial.println("JSON Length: " + packet.length());
    LoRa.beginPacket();
    LoRa.write(destination);
    LoRa.write(localAddress);
    LoRa.print(packet);
    LoRa.endPacket();
    LoRa.receive();
   }
   
void onReceive(int packetSize)
{
if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  recipient = LoRa.read();          // recipient address
  sender = LoRa.read();            // sender address

  String incoming = "";

  while (LoRa.available())
  {
    incoming += (char)LoRa.read();
  }


  rssi=String(LoRa.packetRssi());
  
  packet=incoming;
  gotpacket=true;
}
