#include <ArduinoJson.h>
#include "heltec.h"
#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

String rssi = "RSSI --";
String packSize = "--";
String packet = "NOT INITIALIZED";

byte localAddress = 0xAA;     // address of this device Pump
byte destination = 0xFF;      // destination to send to Trough
int liftpin=12;
int boostpin=13;
int v12pin=36;
int v24pin=37;
float XS = 0.00350;
int reporttimer;
int reportdelay=60000;
int liftdelay=10000;
int boostdelay=45000;
boolean pumping = 0;
int lastcomm = 0;
int commtimeout = 120000;
boolean watercall=false;
unsigned int counter = 0;

byte sensorInterrupt = 39;  // 0 = digital pin 2
byte sensorPin       = 39;

// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;

volatile byte pulseCount;  

float flowRate = 0;
unsigned int flowMilliLitres = 0;
unsigned long totalMilliLitres = 0;

unsigned long oldTime = 0;
unsigned long startflowtime = 0;
unsigned long flowtime = 0;
unsigned long avgflowrate = 0;


bool gotpacket = false; // software flag for LoRa receiver, received data makes it true.



void setup()
{

  Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/, true /*LoRa use PABOOST*/, BAND /*LoRa RF working band*/);
	Heltec.display -> clear();

  //Flowmeter control
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  
	pinMode(liftpin,OUTPUT);
	pinMode(boostpin,OUTPUT);

	LoRa.onReceive(onReceive);
  LoRa.receive();
  displaySendReceive();
  reporttimer=millis();
}


void loop()
{
 if (millis()>reporttimer+reportdelay)
  {
    sendupdate();
  }
  if (pumping){
   if((millis() - oldTime) > 1000)    // Only process counters once per second
    { 
      calcflow();
    } 
  } 
 if(gotpacket)
 {
    gotpacket = false;    
    pumpstate();
    

  }
  if (!gotpacket){
    if (millis() > lastcomm + commtimeout)
    {
      packet="NO COMMUNICATION";
      if (pumping)
      {
        Serial.println("Too long since last packet; shutting off pumps and calling Mom");
        digitalWrite(boostpin,LOW);
        digitalWrite(liftpin,LOW);
        digitalWrite(25,LOW);
        flowtime = startflowtime-millis();
        avgflowrate= (totalMilliLitres/flowtime)/1000;
        pumping=false;
        pulseCount=0;
        sendupdate();
      }
    }
  }
  displaySendReceive();
}

void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}

void displaySendReceive()
{


    Heltec.display -> drawString(0, 0, "Received Packet");
    Heltec.display -> drawString(0, 10, packet);
    Heltec.display -> drawString(0, 20, "With " + rssi + "db");
    Heltec.display -> drawString(0, 50, "Packets rcvd: " + (String)counter);
    Heltec.display -> display();
    delay(100);
    Heltec.display -> clear();
}

void calcflow()
{
    detachInterrupt(sensorInterrupt);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    oldTime = millis();
    flowMilliLitres = (flowRate / 60) * 1000;
    totalMilliLitres += flowMilliLitres;
    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
}
void sendupdate()
   {
    reporttimer = millis();
    float v12  =  analogRead(v12pin)*XS;
    float v24  =  analogRead(v24pin)*XS;
    StaticJsonDocument<200> doc;

    doc["v12"] = (String)v12;
    doc["v24"]=(String)v24;
    doc["ps"]=(String)pumping;
    doc["sig"]=(String)rssi;
    doc["fr"]=(String)avgflowrate;
    
    String packet;
    serializeJson(doc, packet);
    Serial.println("Sending JSON: " + packet);
    Serial.println("JSON Length: " + packet.length());
    LoRa.beginPacket();
    LoRa.write(destination);
    LoRa.write(localAddress);
    LoRa.print(packet);
    LoRa.endPacket();
    LoRa.receive();
   }
   
void pumpstate()
{
  if (watercall) {
      

      if (!pumping){
        pumping=true;
        oldTime = millis();
        pulseCount=0;
        totalMilliLitres=0;
        flowRate=0;
        flowtime=0;
        startflowtime=millis();
        sendupdate();
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "Watercall RECEIVED");
        Heltec.display -> display();
        digitalWrite(25,HIGH);
        Serial.println("Lift pump starting...");
        Heltec.display -> drawString(0, 10, "Lift pump starting...");
        Heltec.display -> display();
        digitalWrite(liftpin,HIGH);
        delay(liftdelay);  
        Serial.println("Boost pump starting...");
        Heltec.display -> drawString(0, 20, "Boost pump starting...");
        Heltec.display -> display();
        digitalWrite(boostpin,HIGH);
        delay(5000);
        Heltec.display -> drawString(0, 30, "Both pumps active.");
        Heltec.display -> display();
        delay(boostdelay);

        
      }
    }
    if (!watercall){
      if (pumping){
        pumping=false;
        flowtime = startflowtime-millis();
        avgflowrate= (totalMilliLitres/flowtime)/1000;
        sendupdate();
        Serial.println("Turn off pump relays and reset flags");

        digitalWrite(boostpin,LOW);
        digitalWrite(liftpin,LOW);
        digitalWrite(25,LOW);

      }
      
    }
}
void onReceive(int packetSize)//LoRa receiver interrupt service
{

    packSize = String(packetSize,DEC);


  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address

  String incoming = "";

  while (LoRa.available())
  {
    incoming += (char)LoRa.read();
  }
  
  if (recipient != localAddress && recipient != 0xFF) {
    Serial.println("This message is not for me.");
    return;                             // skip rest of function
  }
    Serial.println(incoming);
    DynamicJsonDocument doc(1024);    
    deserializeJson(doc, incoming);
    JsonObject root = doc.as<JsonObject>();
    
    for (JsonPair kv : root) {
        String k = kv.key().c_str();
        String v = kv.value().as<char*>();
        if (k=="wl"){
          if (v=="0"){
            watercall=true;
            Serial.println("watercall true");
          }
          else {
            watercall=false;
            Serial.println("watercall false");
          }
        }
    }
    counter++;
    packet=incoming;
    rssi = String(LoRa.packetRssi(), DEC);    
    gotpacket = true;
    lastcomm = millis();    
}
