
#include <ArduinoJson.h>
#include "heltec.h"

#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

byte localAddress = 0xBB;     // address of this device
byte destination = 0xAA;      // destination to send to

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  15        /* Time ESP32 will go to sleep (in seconds) */

int floatpin = 13;             // pin of float switch, to ground, NO
int voltagepin = 2;           // pin for voltage reading
int waterlevel = 1;           // waterlevel good, no pump call
float XS = 0.00350;           //The returned reading is multiplied by this XS to get the battery voltage.

void setup() {
  // Initialize Serial port
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.begin(115200);
  Serial.println("Hi, I'm Awake!");
    if (!digitalRead(floatpin))
    {
      Serial.println("Waterlevel nominal");
      waterlevel=1;
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "Waterlevel Nominal");
    }
    else
    {    
      Serial.println("Waterlevel low");
      waterlevel=0;
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "Waterlevel LOW!");   
    }

   float c  =  analogRead(voltagepin)*XS;
   //Serial.println(analogRead(voltagepin)*XS);
   Heltec.display->drawString(0, 10, "VBAT:");
   Heltec.display->drawString(35, 10, (String)c);
   Heltec.display->drawString(60, 10, " V");
   Heltec.display->display();
   Heltec.display->clear();
   
   StaticJsonDocument<200> doc;

    doc["vss"] = (String)c;
    doc["wl"]=(String)waterlevel;
    String packet;
    serializeJson(doc, packet);
    
    LoRa.beginPacket();
    LoRa.write(destination);
    LoRa.write(localAddress);
    LoRa.print(packet);
    LoRa.endPacket();
    

  Serial.println("Sending JSON: " + packet);
 
  delay(2000); 
  LoRa.end();
  LoRa.sleep();

  delay(100);
  Serial.println("Going to sleep now");
  delay(2);

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_deep_sleep_start();
}

void loop() {
  // not used 
}
